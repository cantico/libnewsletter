��          t      �                      (  &   6     ]     e     j  5   �     �     �  i  �     C     K     Z  !   h     �     �     �  4   �  0   �     $     	   
                                                    API key API secret key API user name Default subscription confirmation page Mailjet Save The email is mandatory The list of subscriptions must be an array of list ID This is a wrong email format YMLP Project-Id-Version: LibNewsletter
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-16 11:18+0100
PO-Revision-Date: 2016-03-16 11:18+0100
Last-Translator: Antoine GALLET <antoine.gallet@cantico.fr>
Language-Team: Cantico <support@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../programs
X-Poedit-KeywordsList: LibNewsletter_translate;LibNewsletter_translate:1,2
X-Generator: Poedit 1.8.7
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: api
 API key API secret key API user name Page de confimation d'inscription Mailjet Enregistrer L'adresse email est obligatoire la liste des inscriptions doit être un tableau d'ID L'adresse email n'est pas formatée correctement YMLP 