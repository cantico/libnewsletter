; <?php/*

[general]
name							="LibNewsletter"
version							="1.2.4"
encoding						="UTF-8"
description						="Newsletter subscription managment with webservices"
description.fr					="Librairie partagée fournissant une API de gestion des listes de diffusions sur des services externes"
long_description.fr             ="README.md"
delete							=1
ov_version						="7.7.0"
php_version						="5.4.0"
mysql_version					="4.1.2"
addon_access_control			=0
author							="Cantico"
icon							="mail_message_new.png"
mysql_character_set_database	="latin1,utf8"
configuration_page				="admin"
mod_curl						="Available"
tags                            ="library,email"

[functionalities]
Mailing							="Available"
;*/?>
