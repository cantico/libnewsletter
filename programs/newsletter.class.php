<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


class Func_Newsletter extends bab_functionality
{
    public function getDescription()
    {
        return 'Newsletter';
    }

    public function getContact()
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }


    /**
     * Create a mailing list
     *
     * @param string $name
     *
     * @return int ID
     */
    public function addList($name)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }


    /**
     * Delete a mailing list
     *
     * @param int $id
     *
     * @return bool
     */
    public function deleteList($id)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }




    /**
     * Rename a mailing list
     *
     * @param int $id
     * @param string $name
     *
     * @return bool
     */
    public function updateList($id, $name)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }

    /**
     * Get all mailling lists, sorted by name
     * keys are mailling list id, values are malling lists names (label visible by user)
     *
     * mandatory method
     *
     * @return array()
     */
    public function getLists()
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }



    /**
     * Create a new contact
     * If the contact allready exists in one of the lists, the action is ignored but contact should be created in the others lists
     * mandatory method
     *
     * @param	string	$email
     * @param	string	$name
     * @param	Array	$lists		list of id
     *
     * @return bool
     */
    public function addContact($email, $name, Array $lists)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }

    /**
     * Get the list where the contact is subscribed
     * return an array where keys are mailling list id, values are malling lists names (label visible by user)
     * return null if the contact does not exists
     * @return array
     */
    public function getContactSubscriptions($email)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }



    /**
     * Subscribe contact to a list
     * Return true if contact subscribed
     * if the contact does not exists, il will be created
     *
     * @throws LibNewsletterException throws an exception if the contact cannot be subscribed
     *
     * @param	string	$email
     * @param	string	$name
     * @param	string	$list		list ID
     * @param	bool	$force		force resubscribe
     */
    public function subscribeContact($email, $name, $list, $force = false)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }



    /**
     * Subscribe multiple contacts to a list
     * Return true if contact subscribed
     * if the contact does not exists, il will be created
     *
     * @param	Array	$contacts[] = [email],[name]
     * @param	string	$list		list ID
     * @param	bool	$force		force resubscribe
     *
     * @return bool
     */
    public function subscribeContacts(Array $contacts, $list, $force = false)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }


    /**
     * Unsubscribe contact from a list
     * Return true if contact unsuscribed
     *
     * @throws LibNewsletterException throws an exception if the contact cannot be unsubscribed
     *
     * @param	string	$email
     * @param	string	$list		list ID
     */
    public function unsubscribeContact($email, $list)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }


    /**
     * Unsubscribe contacts from a list
     * Return true if contact unsuscribed
     *
     * @throws LibNewsletterException throws an exception if the contact cannot be unsubscribed
     *
     * @param	string	$email
     * @param	string	$list		list ID
     * @param	bool	$force		force resubscribe
     */
    public function unsubscribeContacts(Array $contacts, $list, $force = false)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }

    /**
     * Delete contact
     * mandatory method
     * return true if contact removed
     *
     * @param	string 	$email
     * @param	Array	$list		id_list as values, if not set, the contact will be removed from all the lists or completly deleted, if set the contact will be deleted from the specified lists
     *
     * @return bool
     */
    public function removeContact($email, Array $lists = null)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }



    /**
     * Get contacts in a specific list
     *
     * @param int $list
     *
     * @return array [
     *		'id',
     *		'email',
     *		'name'
     * ]
     */
    public function getListContact($list)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }



    /**
     * Update contact email
     *
     * @param string	$old	The old email address
     * @param string	$new	The new email address
     *
     * @return bool
     */
    public function updateContactEmail($old, $new)
    {
        throw new LibNewsletterException(__FUNCTION__.' Not implemented');
    }



    /**
     * Test if the functionality can be used
     * @return bool
     */
    public function isConfigured()
    {
         return false;
    }

}


/**
 *
 *
 */
class LibNewsletterException extends Exception {

}