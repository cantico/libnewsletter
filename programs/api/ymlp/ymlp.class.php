<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
bab_functionality::includeOriginal('Newsletter');

class Func_Newsletter_Ymlp extends Func_Newsletter
{
	private $api = null;
	
	public function getDescription()
	{
		return 'Your mailling list provider http://www.ymlp.com';
	}
	
	
	/**
	 * Test if the functionality can be used
	 * @return bool
	 */
	public function isConfigured()
	{
	    try {
	        $this->API();
	    } catch(LibNewsletterYmlpException $e) {
	        return false;
	    }
	    
	    return true;
	}
	
	
	/**
	 * 
	 * @throws LibNewsletterYmlpException
	 * @return YMLP_API
	 */
	private function API()
	{
		if (null === $this->api)
		{
			require_once dirname(__FILE__).'/YMLP_API.class.php';
			
			$registry = bab_getRegistryInstance();
			$registry->changeDirectory('/LibNewsletter/');
			
			$I = $registry->getValueEx(array('ymlp_api_key', 'ymlp_api_user_name'));
			
			foreach($I as $arr)
			{
				${$arr['key']} = $arr['value'];
			}
			
			if ((!isset($ymlp_api_key))||(!isset($ymlp_api_user_name)))
			{
				throw new LibNewsletterYmlpException('missing YMLP configuration');
			}
			
			
			$this->api = new YMLP_API($ymlp_api_key, $ymlp_api_user_name);
		}
		
		return $this->api;
	}
	
	
	/**
	 * @return array()
	 */
	public function getLists()
	{
		$api = $this->API();
		$response = $api->GroupsGetList();
		
		if ($api->ErrorMessage)
		{
			throw new LibNewsletterYmlpException($api->ErrorMessage);
		}
	
		if (!$response)
		{
			return array();
		}

		$return = array();
	
		foreach($response as $list)
		{
			$return[$list['ID']] = bab_getStringAccordingToDataBase($list['GroupName'], 'UTF-8');
		}
	
		return $return;
	}
	
	
	
	
	/**
	 * Create a new contact
	 *
	 * mandatory method
	 *
	 * @param	string	$email
	 * @param	Array	$lists		list of id
	 * 
	 * @return bool
	 */
	public function addContact($email, Array $lists)
	{
		$api = $this->API();
		$response = $api->ContactsAdd($email, '', implode(',',$lists));
		
		if (0 === $response)
		{
			return true;
		}
		
		if ($api->ErrorMessage)
		{
			throw new LibNewsletterYmlpException($api->ErrorMessage);
		}
		
		return false;
	}
	
	
	
	
	/**
	 * Get the list where the contact is subscribed
	 * return an array where keys are mailling list id, values are malling lists names (label visible by user)
	 * return null if the contact does not exists
	 * @return array
	 */
	public function getContactSubscriptions($email)
	{
		$api = $this->API();
		$response = $api->ContactsGetContact(bab_convertStringFromDatabase($email, 'UTF-8'));
	
		$return = array();
		
		foreach($response as $keyname => $value)
		{
			if (1 === (int) $value && 'GROUP' === substr($keyname,0,5))
			{
				$id_group = (int) substr($keyname,5);
				$return[$id_group] = '';
			}
		}
		
		if (0 === count($return))
		{
			return $return;
		}
		
		
		$all = $this->getLists();
		
		foreach($return as $id_group => $empty)
		{
			$return[$id_group] = $all[$id_group];
		}
		
		
		return $return;
	}
	
	
	
	
	/**
	 * Subscribe contact to a list
	 * Return true if contact subscribed
	 * if the contact does not exists, il will be created
	 *
	 * @throws LibNewsletterException throws an exception if the contact cannot be subscribed
	 *
	 * @param	string	$email
	 * @param	string	$list		list ID
	 */
	public function subscribeContact($email, $list)
	{
		// @TODO throw an exception if contact allready in list
		
		$this->addContact($email, array($list));
	}
	
	
	/**
	 * Unsubscribe contact from a list
	 * Return true if contact unsuscribed
	 *
	 * @throws LibNewsletterException throws an exception if the contact cannot be unsubscribed
	 *
	 * @param	string	$email
	 * @param	string	$list		list ID
	 */
	public function unsubscribeContact($email, $list)
	{
		// YMLP does not allow unsubscribtion within one group only
		
		return $this->removeContact($email, array($list));
	}
	
	
	
	
	
	
	
	
	/**
	 * Delete contact
	 * return true if contact removed
	 *
	 * @param	string $email
	 * @param	Array	$list		id_list as values, if not set, the contact will be removed from all the lists or completly deleted, if set the contact will be deleted from the specified lists
	 *
	 * @return bool
	 */
	public function removeContact($email, Array $lists = null)
	{
		$api = $this->API();
	
		if (null === $lists)
		{
			$lists = array_keys($this->getLists());
		}
	
		$response = $api->ContactsDelete($email, implode(',', $lists));
		
		if (0 === $response)
		{
			return true;
		}
		
		if ($api->ErrorMessage)
		{
			throw new LibNewsletterYmlpException($api->ErrorMessage);
		}
		
		return false;
	}
}



class LibNewsletterYmlpException extends LibNewsletterException {
	
}