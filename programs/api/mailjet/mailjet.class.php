<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
bab_functionality::includeOriginal('Newsletter');

class Func_Newsletter_Mailjet extends Func_Newsletter
{
    private $api = null;

    public function getDescription()
    {
        return 'Mailjet http://www.mailjet.com';
    }


    /**
     * @return bool
     */
    public function isConfigured()
    {
        try {
            $this->API();
        } catch(LibNewsletterMailjetException $e) {
            return false;
        }

        return true;
    }


    private function includeFiles()
    {
        require_once dirname(__FILE__).'/autoload.php';
    }


    /**
     *
     * @throws LibNewsletterMailjetException
     * @return Mailjet
     */
    private function API()
    {
        if (null === $this->api)
        {
            if (!class_exists('\\Mailjet\\Client')) {
                $this->includeFiles();
            }

            $registry = bab_getRegistryInstance();
            $registry->changeDirectory('/LibNewsletter/');

            $I = $registry->getValueEx(array('mailjet_api_key', 'mailjet_api_secret_key'));

            foreach($I as $arr)
            {
                ${$arr['key']} = $arr['value'];
            }

            if ((!isset($mailjet_api_key)) || (!isset($mailjet_api_secret_key)))
            {
                throw new LibNewsletterMailjetException('missing Mailjet configuration');
            }


            $this->api = new \Mailjet\Client($mailjet_api_key, $mailjet_api_secret_key);
            //$this->api->debug = 0;
        }

        return $this->api;
    }



    /**
     * @param string $name
     *
     * @return int ID
     */
    public function addList($name)
    {
        //SEARCH EXISTING LIST WITH SAME NAME FIRST
        $lists = $this->getLists();
        foreach($lists as $id => $listName) {
            if($listName == $name) {
                return $id;
            }
        }

        $api = $this->API();

        $body = [
            'Name' => $name
        ];

        $response = $api->post(
            \Mailjet\Resources::$Contactslist,
            ['body' => $body]
        );

        if (!$response->success())
        {
            return false;
        }

        $data = $response->getData();

        return $data[0]['ID'];
    }


    /**
     * @param int ID
     *
     * @return bool
     */
    public function deleteList($id)
    {
        $api = $this->API();

        $response = $api->delete(
            \Mailjet\Resources::$Contactslist,
            ['ID' => $id]
        );

        if (!$response->success())
        {
            return false;
        }

        return true;
    }


    /**
     * @param int ID
     * @param string $name
     *
     * @return bool
     */
    public function updateList($id, $name)
    {
        $api = $this->API();

        $body = [
            'Name' => $name
        ];

        $response = $api->put(
            \Mailjet\Resources::$Contactslist,
            [
                'ID' => $id,
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            return false;
        }

        return true;
    }



    /**
     *
     * @return array()
     */
    public function getLists($offset = 0)
    {
        $api = $this->API();
        $response = $api->get(\Mailjet\Resources::$Contactslist, [
            'filters' => [
                'Limit' => '1000',
                'Offset' => $offset
            ]
        ]);

        if (!$response->success())
        {
            return array();
        }

        $return = array();
        foreach($response->getData() as $list)
        {
            $return[$list['ID']] = bab_getStringAccordingToDataBase($list['Name'], 'UTF-8');
        }

        if (count($return) >= 1000) {
            $more = $this->getLists(($offset+1000));
            $return = $return+$more;
        }

        return $return;
    }



    /**
     * Create a new contact
     *
     * mandatory method
     *
     * @param	string	$email
     * @param	Array	$lists		list of id
     *
     * @return bool
     */
    public function addContact($email, $name, Array $lists)
    {
        if (empty($lists))
        {
            throw new LibNewsletterMailjetException('For mailjet at least one mailling list ID must be specified to create a contact');
        }

        $status = true;
        foreach($lists as $id_list)
        {
            try {
                $this->subscribeContact($email, $name, $id_list);
            } catch(LibNewsletterException $e)
            {
                $status = false;
            }
        }

        return $status;
    }



    /**
     * Get the list where the contact is subscribed
     * return an array where keys are mailling list id, values are malling lists names (label visible by user)
     * return null if the contact does not exists
     * @return array
     */
    public function getContactSubscriptions($email, $offset = 0)
    {
        $api = $this->API();
        $response = $api->get(
            \Mailjet\Resources::$ContactGetcontactslists,
            [
                'id' => bab_convertStringFromDatabase($email, 'UTF-8'),
                'filters' => [
                    'Limit' => '1000',
                    'Offset' => $offset
                ]
            ]
        );

        if (!$response->success())
        {
            return null;
        }

        $return = array();

        foreach($response->getData() as $l)
        {
            if (!$l['IsUnsub'] && $l['IsActive'])
            {
                $listInfo = $this->getListInfo($l['ListID']);
                $return[$l['ListID']] = $listInfo['name'];
            }
        }

        if (count($return) >= 1000) {
            $more = $this->getContactSubscriptions($email, ($offset+1000));
            $return = $return+$more;
        }

        return $return;
    }


    public function getListInfo($list)
    {
        $api = $this->API();
        $response = $api->get(
            \Mailjet\Resources::$Contactslist,
            [
                'id' => $list
            ]
        );

        if (!$response->success())
        {
            return array();
        }

        $data = $response->getData()[0];

        return array(
            'id' => $data['ID'],
            'createdOn' => $data['CreatedAt'],
            'deleted' => $data['IsDeleted'],
            'name' => bab_getStringAccordingToDataBase($data['Name'], 'UTF-8'),
            'recipientCount' => $data['SubscriberCount']
        );
    }


    public function getListContact($list, $offset = 0)
    {
        $api = $this->API();
        $response = $api->get(
            \Mailjet\Resources::$Contact,
            [
                'filters' => [
                    'ContactsList' => $list,
                    'Limit' => '1000',
                    'Offset' => $offset
                ]
            ]
        );

        if (!$response->success())
        {
            return array();
        }

        $datas = $response->getData();
        $contacts = array();
        foreach($datas as $contact) {
            $contacts[$contact['ID']] = [
                'id' => $contact['ID'],
                'email' => $contact['Email'],
                'name' => $contact['Name']
            ];
        }

        if (count($contacts) >= 1000) {
            $more = $this->getListContact($list, ($offset+1000));
            $contacts = $contacts+$more;
        }

        return $contacts;
    }




    /**
     * Subscribe contact to a list
     * Return true if contact subscribed
     * if the contact does not exists, il will be created
     *
     * @throws LibNewsletterException throws an exception if the contact cannot be subscribed
     *
     * @param	string	$email
     * @param	string	$name
     * @param	string	$list		list ID
     * @param	bool	$force		force resubscribe
     */
    public function subscribeContact($email, $name, $list, $force = false)
    {
        $api = $this->API();

        $action = 'addnoforce';
        if ($force) {
            $action = 'addforce';
        }

        $body = [
            'Email' => bab_convertStringFromDatabase($email, 'UTF-8'),
            'Name' => bab_convertStringFromDatabase($name, 'UTF-8'),
            'Action' => $action
        ];

        $response = $api->post(
            \Mailjet\Resources::$ContactslistManagecontact,
            [
                'id' => $list,
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            throw new LibNewsletterMailjetException($response->getStatus());
        }

        return true;
    }




    /**
     * Subscribe multiple contacts to a list
     * Return true if contact subscribed
     * if the contact does not exists, il will be created
     *
     * @throws LibNewsletterException throws an exception if the contact cannot be subscribed
     *
     * @param	Array	$contacts[] = [email],[name]
     * @param	string	$list		list ID
     * @param	bool	$force		force resubscribe
     */
    public function subscribeContacts($contacts, $list, $force = false)
    {
        $api = $this->API();

        $action = 'addnoforce';
        if ($force) {
            $action = 'addforce';
        }

        $body = [
            'Action' => $action,
            'Contacts' => []
        ];

        foreach ($contacts as $contact) {
            $body['Contacts'][] = [
                'Email' => $contact['email'],
                'Name' => $contact['name']
            ];
        }

        $response = $api->post(
            \Mailjet\Resources::$ContactslistManagemanycontacts,
            [
                'id' => $list,
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            throw new LibNewsletterMailjetException($response->getStatus());
        }

        return true;
    }





    /**
     * Unsubscribe contact from a list
     * Return true if contact unsuscribed
     *
     * @throws LibNewsletterException throws an exception if the contact cannot be unsubscribed
     *
     * @param	string	$email
     * @param	string	$list		list ID
     */
    public function unsubscribeContact($email, $list, $force = true)
    {

        $api = $this->API();

        $action = 'unsub';
        if ($force) {
            $action = 'remove';
        }

        $body = [
            'Action' => $action,
            'Email' => bab_convertStringFromDatabase($email, 'UTF-8')
        ];

        $response = $api->post(
            \Mailjet\Resources::$ContactslistManagecontact,
            [
                'id' => $list,
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            throw new LibNewsletterMailjetException($response->getStatus());
        }

        return true;
    }




    /**
     * Subscribe multiple contacts to a list
     * Return true if contact subscribed
     * if the contact does not exists, il will be created
     *
     * @throws LibNewsletterException throws an exception if the contact cannot be subscribed
     *
     * @param	Array	$contacts[] = [email],[name]
     * @param	string	$list		list ID
     * @param	bool	$force		force resubscribe
     */
    public function unsubscribeContacts($contacts, $list, $force = false)
    {
        $api = $this->API();

        $action = 'unsub';
        if ($force) {
            $action = 'remove';
        }

        $body = [
            'Action' => $action,
            'Contacts' => []
        ];

        foreach ($contacts as $contact) {
            $body['Contacts'][] = [
                'Email' => $contact['email'],
                'Name' => $contact['name']
            ];
        }

        $response = $api->post(
            \Mailjet\Resources::$ContactslistManagemanycontacts,
            [
                'id' => $list,
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            throw new LibNewsletterMailjetException($response->getStatus());
        }

        return true;
    }





    /**
     * Unsubscribe contact from a list
     * Return true if contact unsuscribed
     *
     * @throws LibNewsletterException throws an exception if the contact cannot be unsubscribed
     *
     * @param	string	$email
     * @param	string	$list		list ID
     */
    public function isSubscribedContact($email, $list)
    {
        $api = $this->API();
        $response = $api->get(
            \Mailjet\Resources::$ContactGetcontactslists,
            [
                'id' => $email
            ]
        );

        if (!$response->success())
        {
            return array();
        }

        $datas = $response->getData();
        $contacts = array();
        foreach($datas as $subscription) {
            if($subscription['ListID'] == $list) {
                if($subscription['IsActive'] === false || $subscription['IsUnsub'] === true) {
                    return false;
                } else {
                    return true;
                }
            }
        }

        return false;
    }




    /**
     * Delete contact
     * return true if contact removed
     *
     * @param	string $email
     * @param	Array	$list		id_list as values, if not set, the contact will be removed from all the lists
     *
     * @return bool
     */
    public function removeContact($email, Array $lists = null)
    {

        $api = $this->API();

        if (null === $lists)
        {
            $lists = array_keys($this->getLists());
        }
        if (empty($lists)) {
            return true;
        }

        $body = [
            'ContactsLists' => array()
        ];

        foreach($lists as $id_list)
        {
            $body['ContactsLists'][] = [
                'ListID' => $id_list,
                'Action' => 'remove'
            ];
        }

        $response = $api->post(
            \Mailjet\Resources::$ContactManagecontactslists,
            [
                'id' => bab_convertStringFromDatabase($email, 'UTF-8'),
                'body' => $body
            ]
        );

        return $response->success();
    }

    /**
     * Update contact email
     *
     * @param string	$old	The old email address
     * @param string	$new	The new email address
     *
     * @return bool
     */
    public function updateContactEmail($old, $new)
    {

        $api = $this->API();

        $body = [
            'Email' => bab_convertStringFromDatabase($new, 'UTF-8')
        ];

        $response = $api->put(
            \Mailjet\Resources::$Contact,
            [
                'id' => $old,
                'body' => $body
            ]
        );

        if (!$response->success())
        {
            throw new LibNewsletterMailjetException($response->getStatus());
        }

        return true;
    }

    public function getAsyncJobs()
    {
        $api = $this->API();

        $response = $api->get(
            \Mailjet\Resources::$Batchjob,
            [
                'filters' => [
                    'Limit' => '1000'
                ]
            ]
        );

        if (!$response->success())
        {
            throw new LibNewsletterMailjetException($response->getStatus());
        }

        return $response->getData();
    }
}



class LibNewsletterMailjetException extends LibNewsletterException {

}